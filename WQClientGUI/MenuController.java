import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Controller per la view del menu
public class MenuController {
	@FXML private Label usernameLbl;
	@FXML private Button welcomeBtn;
	@FXML private Button aggiungiAmicoBtn;
	@FXML private Button listaAmiciBtn;
	@FXML private Button punteggioBtn;
	@FXML private Button classificaBtn;
	@FXML private Button logoutBtn;
	@FXML private AnchorPane anchor;
	@FXML private AnchorPane changeAnchor;
	@FXML private AnchorPane notifyPane;
	@FXML private AnchorPane menu;

	private Connection connection;
	private InetAddress indResp;
	private int portResp;
	private HashMap<String, String> attrNotification;
    
	// Metodo collegato al bottone sfidaBtn per tornare all'introduzione da cui è possibile lanciare una sfida
	@FXML
    private void welcome() throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.INTRODUZIONE_FXML));
    	changeAnchor.getChildren().clear();
    	changeAnchor.getChildren().add(fxmlLoader.load());

    	IntroduzioneController con = fxmlLoader.getController();
    	con.setConnection(connection);
    	con.changePane(anchor);
    }
	
	// Metodo collegato al bottone aggiungiAmicoBtn per aggiungere un amico
    @FXML
    private void aggiungiAmico() throws IOException {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.AGGIUNGIAMICO_FXML));
    	changeAnchor.getChildren().clear();
    	changeAnchor.getChildren().add(fxmlLoader.load());

    	AggiungiAmicoController con = fxmlLoader.getController();
    	con.setConnection(connection);
    }
    
    // Metodo collegato al bottone listaAmiciBtn per recuperare la lista degli amici
    @FXML
    private void listaAmici() throws IOException {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.LISTAAMICI_FXML));
    	changeAnchor.getChildren().clear();
    	changeAnchor.getChildren().add(fxmlLoader.load());

    	ListController con = fxmlLoader.getController();
    	
    	Message msg = connection.lista_amici();
    	
		if (msg.getCode().equals(Costanti.okCode)) {
			Gson gson = new Gson();
			Type type = new TypeToken<ArrayList<String>>(){}.getType();
			ArrayList<String> lista = gson.fromJson(msg.getMessageBody(), type);
			
	        con.insertList(lista);
		} else {
			// Messaggio di errore
			con.placeholder(msg.getMessageBody());
		}
    }
    
    // Metodo collegato al bottone punteggioBtn per recuperare il punteggio
    @FXML
    private void mostraPunteggio() throws IOException {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.PUNTEGGIO_FXML));
    	changeAnchor.getChildren().clear();
    	changeAnchor.getChildren().add(fxmlLoader.load());

    	PunteggioController con = fxmlLoader.getController();
    	Message msg = connection.mostra_punteggio();
    	
		if (msg.getCode().equals(Costanti.okCode)) {
	        con.setLabels(msg.getAttributi());
		}
    }
    
    // Metodo collegato al bottone classificaBtn per recuperare la classifica
    @FXML
    private void mostraClassifica() throws IOException {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.LISTAAMICI_FXML));
    	changeAnchor.getChildren().clear();
    	changeAnchor.getChildren().add(fxmlLoader.load());
    	
    	ListController con = fxmlLoader.getController();
    	JSONArray list = connection.mostra_classifica();
    	con.insertClassifica(list, connection.getNickUtente());
    }
    
    // Metodo collegato al bottone logoutBtn per effettuare il logout
    @FXML
    private void logout() throws IOException {
    	// Esecuzione del Logout
    	connection.logout();
    	
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.LOG_FXML));
    	anchor.getChildren().clear();
    	anchor.getChildren().add(fxmlLoader.load());

    	LoginRegController con = fxmlLoader.getController();
    	con.changePane(anchor);
		con.setConnection(connection);
    }
	
	// Metodo per visualizzare una notifica/richiesta di sfida
	public void viewNotification(Message msg, InetAddress indResp, int portResp) {
		this.indResp = indResp;
		this.portResp = portResp;
		this.attrNotification = msg.getAttributi();
		Label msgLbl = (Label) notifyPane.lookup("#msgLbl");
		msgLbl.setText(msg.getMessageBody());
		notifyPane.setVisible(true);
		menu.setDisable(true);
		changeAnchor.setDisable(true);
		
		// Avvio di un thread per gestire la chiusura a tempo della notifica
		Thread thread = new Thread(() -> {
            try {
                Thread.sleep(Integer.parseInt(attrNotification.get(Costanti.TEMPORISPOSTA)));
                if (notifyPane.isVisible()) {
                	close();
                }
            } catch (Exception exp) {
                exp.printStackTrace();
            }
        });
        thread.setDaemon(true);
        thread.start();
	}
	
	// Metodo collegato al bottone acceptBtn per accettare una sfida
	@FXML
    private void accept() throws IOException {
		Message msg = new Message(Costanti.challengeAcceptCode, "Sfida accettata", null);
		
    	// Invio della risposta
    	connection.sendResponse(msg, indResp, portResp);
    	
    	// Chiusura della notifica
    	close();
		
		Message rispostaSfida = connection.setupSfida();
		
		if (rispostaSfida.getCode().equals(Costanti.okCode)) {
			System.out.println("WQClientGUI | Sfida in corso");
			menu.setDisable(true);
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.GAME_FXML));
	    	changeAnchor.getChildren().clear();
	    	try {
				changeAnchor.getChildren().add(fxmlLoader.load());
				GameController con = fxmlLoader.getController();
		    	con.setConnection(connection);
		    	con.changePane(anchor);
		    	con.setLabelSfidato(attrNotification.get(Costanti.NICKAMICO));
			} catch (IOException e) {
				System.err.println("WQClientGUI | Errore nella sfida " + e.toString());
			}
    	} else {
    		// La sfida non è più possibile
    		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.SFIDA_FXML));
        	changeAnchor.getChildren().clear();
        	changeAnchor.getChildren().add(fxmlLoader.load());

        	SfidaController con = fxmlLoader.getController();
        	con.setConnection(connection);
        	con.changePane(anchor);
        	con.errorOccured(rispostaSfida);
    	}
    }
	
	// Metodo collegato al bottone declineBtn per rifiutare una sfida
	@FXML
    private void decline() throws IOException {
		Message msg = new Message(Costanti.challengeDeclineCode, "Sfida rifiutata", null);
		
		// Invio della risposta
    	connection.sendResponse(msg, indResp, portResp);
    	
    	// Chiusura della notifica
    	close();
    }
	
	// Metodo collegato al bottone closeBtn per chiudere la notifica
	@FXML
    private void close() throws IOException {
    	// Esecuzione del Logout
		notifyPane.setVisible(false);
		menu.setDisable(false);
		changeAnchor.setDisable(false);
    }
	
	// Metodo per inizializzare gli elementi grafici
	public void changePane(AnchorPane anchorPane) {
		anchor = anchorPane;
		usernameLbl.setText(connection.getNickUtente());
		try {
			close();
			welcome();
		} catch (IOException e) {
			System.err.println("WQClientGUI | Errore " + e.toString());
		}
	}
	
	// Metodo per inizializzare l'oggetto che gestisce la connessione al server
	public void setConnection(Connection connect) {
		connection = connect;
	}
}
