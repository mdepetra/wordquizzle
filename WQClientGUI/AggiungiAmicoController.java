import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Controller per la vista che permette l'aggiunta di un amico
public class AggiungiAmicoController {
	@FXML private Label responseLbl;
	@FXML private Button sendBtn;
	@FXML private TextField usernameTF;
	private Connection connection;

	// Metodo collegato al bottone sendBtn per aggiungere un amico
	@FXML
	public void aggiungiAmico() throws IOException {
		if (!usernameTF.getText().isBlank()) {
			// Se è stato specificato un nome valido nell'usernameTextField
			Message msg = connection.aggiungi_amico(usernameTF.getText());
			
			// In base al codice del messaggio di risposta, si imposta un certo style per la label di risposta
			if (msg.getCode().equals(Costanti.okCode))
				responseLbl.setStyle(Costanti.styleOk + Costanti.styleOkFull);
			else
				responseLbl.setStyle(Costanti.styleError + Costanti.styleErrorFull);
			
			// Si comunica l'esito dell'operazione all'utente
			responseLbl.setText(msg.getMessageBody());
			usernameTF.clear();
		} else {
			// Se usernameTF non contiene valori validi
			responseLbl.setStyle(Costanti.styleError + Costanti.styleErrorFull);
			responseLbl.setText("Inserire il nome dell'amico da aggiungere");
		}
	}
    
	// Metodo per inizializzare l'oggetto che gestisce la connessione al server
	public void setConnection(Connection connect) {
		connection = connect;
	}
}
