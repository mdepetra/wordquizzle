/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Questo file contiene tutti i valori costanti usati nel WQClient
public class Costanti {
	// Argomenti per la connessione al server
	public static int portRMI = 6789;
	public static int portTCP = 6790;
	public static String hostnameServer = "localhost";
	public static int BUF_SZ = 512;				// Dimensione degli array di byte
	
	// Codici per i messaggi operazione dal client
	public static String LOGIN = "login";
	public static String LOGOUT = "logout";
	public static String AGGIUNGI_AMICO = "aggiungi_amico";
	public static String LISTA_AMICI = "lista_amici";
	public static String SFIDA = "sfida";
	public static String MOSTRA_PUNTEGGIO = "mostra_punteggio";
	public static String MOSTRA_CLASSIFICA = "mostra_classifica";
	
	// Codici per i messaggi di risposta dal server
	public static String okCode = "200";
	public static String errorCode = "401";
	public static String challengeRequestCode = "302";
	public static String challengeAcceptCode = "303";
	public static String challengeDeclineCode = "304";
	public static String readyCode = "305";
	public static String nextWordCode = "306";
	public static String endWordCode = "307";
	public static String sfidaResultCode = "308";
	
	// Key per attributi
	public static String NICKUTENTE = "nickUtente";
	public static String PASSWORD = "password";
	public static String NICKAMICO = "nickAmico";
	public static String PUNTEGGIO = "punteggio";
	public static String SFIDEGIOCATE = "sfideGiocate";
	public static String CORRETTE = "paroleCorrette";
	public static String ERRATE = "paroleErrate";
	public static String NONFORNITE = "paroleNonFornite";
	public static String PAROLA = "parola";
	public static String TEMPORISPOSTA = "T1";
	public static String TEMPOSFIDA = "T2";
	public static String PUNTIAVVERSARIO = "puntiAvversario";
	public static String EXTRA = "extra";
	
	// Stili per i messaggi di risposta
	public static String styleOk = "-fx-text-fill: #0ea300;";
	public static String styleOkFull = "-fx-background-color: #c4e4d3; -fx-border-color: #0ea300; -fx-border-width: 3px; -fx-background-radius: 15px; -fx-border-radius: 15px;";
	public static String styleError = "-fx-text-fill: #a30000;";
	public static String styleErrorFull = "-fx-background-color: #c48b8b; -fx-border-color: #a30000; -fx-border-width: 3px; -fx-background-radius: 15px; -fx-border-radius: 15px;";
	
	// Nomi dei file fxml
	public static String LOG_FXML = "views/loginreg.fxml";
	public static String MENU_FXML = "views/menu.fxml";
	public static String INTRODUZIONE_FXML = "views/introduzione.fxml";
	public static String AGGIUNGIAMICO_FXML = "views/aggiungiAmico.fxml";
	public static String LISTAAMICI_FXML = "views/lista.fxml";
	public static String PUNTEGGIO_FXML = "views/punteggio.fxml";
	public static String CLASSIFICA_FXML = "views/classifica.fxml";
	public static String SFIDA_FXML = "views/sfida.fxml";
	public static String GAME_FXML = "views/game.fxml";
	public static String CSS = "views/style.css";
	
	// Costruttore
	private Costanti() {
        // restrict instantiation
	}
	
}
