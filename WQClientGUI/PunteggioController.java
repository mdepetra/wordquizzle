import java.util.HashMap;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Controller per visualizzare il punteggio dell'utente e gli altri dati statistici
public class PunteggioController {
	@FXML private Label punteggioLbl;
	@FXML private Label corretteLbl;
	@FXML private Label sfideLbl;
	@FXML private Label errateLbl;
	@FXML private Label nonForniteLbl;

	// Metodo per inizializzare gli elementi grafici
	public void setLabels(HashMap<String, String> attr) {
		punteggioLbl.setText(attr.get(Costanti.PUNTEGGIO));
		sfideLbl.setText(attr.get(Costanti.SFIDEGIOCATE));
		corretteLbl.setText(attr.get(Costanti.CORRETTE));
		errateLbl.setText(attr.get(Costanti.ERRATE));
		nonForniteLbl.setText(attr.get(Costanti.NONFORNITE));
	}
}
