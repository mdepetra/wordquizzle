import java.io.IOException;
import java.util.HashMap;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Controller per la vista della sfida (quella per la sfida vera e propria)
public class GameController {
	private AnchorPane anchor;
	@FXML private AnchorPane changeAnchor;
	@FXML private Label user1Lbl;
	@FXML private Label user2Lbl;
	@FXML private AnchorPane paroleAnchor;
	@FXML private Label challengeLbl;
	@FXML private Label parolaLbl;
	@FXML private TextField parolaTF;
	@FXML private AnchorPane esitoAnchor;
	@FXML private Label responseLbl;
	@FXML private Connection connection;
	@FXML private Label corretteLbl;
	@FXML private Label errateLbl;
	@FXML private Label nonForniteLbl;
	@FXML private Label avversarioLbl;
	@FXML private Label puntiExtraLbl;
	@FXML private Label risultatoLbl;
	@FXML private AnchorPane endAnchor;
	@FXML private Label endLbl;
	@FXML private Button checkBtn;
	@FXML private Button terminaBtn;
	
	// Metodo collegato al bottone sendBtn per inviare la parola tradotta
	@FXML
    private void sendNextWord() throws IOException {
		// Creazione del messaggio di richiesta
    	Message msg = new Message(Costanti.nextWordCode, parolaTF.getText(), null);
		connection.sendOnChallengeSocket(msg);
		getNextWord();
    }
	
	// Metodo collegato al bottone checkBtn per consultare l'esito della sfida
	@FXML
    private void check() throws IOException {
		// Creazione del messaggio di richiesta
    	Message msg = new Message(Costanti.endWordCode, "Risultato?", null);
    	connection.sendOnChallengeSocket(msg);
		getNextWord();
    }
	
	// Metodo collegato al bottone terminaBtn per terminare la sfida
	@FXML
    private void terminaSfida() throws IOException {
		connection.endSfida();
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.INTRODUZIONE_FXML));
    	changeAnchor.getChildren().clear();
    	changeAnchor.getChildren().add(fxmlLoader.load());

    	IntroduzioneController con = fxmlLoader.getController();
    	
    	con.changePane(anchor);
    	con.setConnection(connection);
	}
	
	// Metodo per inizializzare gli elementi grafici
	public void changePane(AnchorPane anchorPane) {
		anchor = anchorPane;
		user1Lbl.setText(connection.getNickUtente());
		parolaTF = (TextField) paroleAnchor.lookup("#parolaTF");
		esitoAnchor.setVisible(false);
		endAnchor.setVisible(false);
		
		// Richiesta della prima parola
		getNextWord();
	}

	// Metodo per inizializzare l'oggetto che gestisce la connessione al server
	public void setConnection(Connection connect) {
		connection = connect;
	}

	// Metodo per impostare il nome dell'amico
	public void setLabelSfidato(String nickAmico) {
		user2Lbl.setText(nickAmico);
	}
	
	// Metodo per recuperare la parola successiva o il risultato della sfida
	private void getNextWord() {
		Message msg = connection.nextWord();
    	parolaTF.clear();
    	if (msg.getCode().equals(Costanti.nextWordCode)) {
    		// Parola da tradurre
    		HashMap<String, String> attr = msg.getAttributi();
    		challengeLbl.setText(msg.getMessageBody());
    		parolaLbl.setText(attr.get(Costanti.PAROLA));
    		parolaTF.setPromptText(attr.get(Costanti.PAROLA) + " in ENGLISH");
    	} else if (msg.getCode().equals(Costanti.endWordCode)) {
    		// Parole Terminate
    		paroleAnchor.setVisible(false);
    		endAnchor.setVisible(true);
    		endLbl.setText(msg.getMessageBody());
    	} else if (msg.getCode().equals(Costanti.sfidaResultCode)) {
    		// Esito della sfida e sua elaborazione
    		paroleAnchor.setVisible(false);
    		if (endAnchor.isVisible())
    			endAnchor.setVisible(false);
    		esitoAnchor.setVisible(true);
    		responseLbl.setText(msg.getMessageBody());
    		
    		HashMap<String, String> attr = msg.getAttributi();
    		risultatoLbl.setText(attr.get(Costanti.PUNTEGGIO));
			corretteLbl.setText(attr.get(Costanti.CORRETTE));
			errateLbl.setText(attr.get(Costanti.ERRATE));
			nonForniteLbl.setText(attr.get(Costanti.NONFORNITE));
			avversarioLbl.setText(attr.get(Costanti.PUNTIAVVERSARIO));
			
			if (attr.get(Costanti.EXTRA) != null) {
				puntiExtraLbl.setVisible(true);
				puntiExtraLbl.setText("Hai guadagnato " + attr.get(Costanti.EXTRA) + " punti extra, per un totale di " + (Integer.parseInt(attr.get(Costanti.PUNTEGGIO)) + Integer.parseInt(attr.get(Costanti.EXTRA))) + " punti!");
			}
			
    		((AnchorPane) anchor.lookup("#menu")).setDisable(false);
    	}
	}
}
