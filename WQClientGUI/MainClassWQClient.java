import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe Main del WQClient
public class MainClassWQClient extends Application {
	private String name = "Word Quizzle";
	private Stage stage;
	
	public static void main(String[] args) {
		launch(args);
	}

	// Metodo invocato dalla lauch 
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(Costanti.LOG_FXML));
		AnchorPane root = (AnchorPane) loader.load();
		
		LoginRegController con = loader.getController();
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource(Costanti.CSS).toExternalForm());
		
		Connection connection = new Connection();
		con.setConnection(connection);
		con.changePane((AnchorPane) root);
		stage.setTitle(name);
		stage.setScene(scene);
		stage.setResizable(false);
		
		stage.setOnCloseRequest(e -> {
			connection.logout();
	        Platform.exit();
	        System.exit(0);
	    });
	    
		stage.show();
    }
}
