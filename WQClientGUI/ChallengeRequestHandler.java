import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import javafx.application.Platform;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza il gestore delle richeiste di sfida
public class ChallengeRequestHandler implements Runnable {
	private DatagramSocket serverSocket;
	private MenuController menuController;
	private InetAddress indResp;
	private int portResp; 
	
	// Costruttore
	public ChallengeRequestHandler(DatagramSocket serverSocket, MenuController con) {
		this.serverSocket = serverSocket;
		this.menuController = con;
	}
	
	@Override
	public void run() {
		try {
			byte[] receiveData = new byte[Costanti.BUF_SZ];
			
			DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
			
			while(!Thread.currentThread().isInterrupted()) {
				// Ricezione di un paccketto dal client
				serverSocket.receive(receivePacket);
				
				// Trasformazione del campo DATA in una stringa
				String bytesToString = new String(receivePacket.getData(), 0, receivePacket.getLength(), "US-ASCII");
				
				// Elaborazione del messaggio ricevuto
				Message msg = new Message().messageFromString(bytesToString);
				
				// Configurazione della connessione in corso
				if (msg.getCode().equals(Costanti.challengeRequestCode)) {
					// C'è una richiesta di sfida
					indResp = receivePacket.getAddress();
					portResp = receivePacket.getPort();
						
					Platform.runLater(new Runnable() {
						@Override
			            public void run() {
							// Si visualizza la notifica all'utente
							menuController.viewNotification(msg, indResp, portResp);
			            }
			          });
				}
			}
		} catch (IOException e) {
			System.err.println("WQClientGUI | Errore " + e.toString());
		} finally {
			if (serverSocket != null)
				serverSocket.close();
		}
		
		System.out.println("WQClientGUI | CHIUSO il thread per la gestione delle richieste di sfida");
	}
	
	
}
