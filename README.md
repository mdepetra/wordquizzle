# <img align="middle" width="5%" height="5%" src="/WQClientGUI/views/logo.png"> &nbsp;WordQuizzle

WordQuizzle è un sistema di sfide di traduzione italiano-inglese tra utenti registrati al servizio.

WordQuizzle consente la gestione di una rete sociale tra i suoi iscritti.

Gli utenti registrati sfidano i propri amici ad una gara il cui scopo è quello di tradurre in inglese il maggiore numero di parole italiane proposte dal servizio e verificate con il confronto delle traduzioni eseguite usando le API del servizio gratuito [MyMemory](https://mymemory.translated.net/doc/spec.php).

L’applicazione è implementata in JAVA secondo una architettura client-server. 

## WQServer
WQServer è il server che implementa le funzionalità offerte dal sistema WordQuizzle.
Al suo avvio, il WQServer effettua le seguenti operazioni:
1. Creazione del servizio di Registrazione implementato mediante RMI.
2. Creazione di un thread che controlla ogni 5 minuti la disponibilità del servizio di traduzione.
3. Creazione di una ServerSocket per l’accept di nuove connessione. Per ogni nuova connessione, viene attivato un task ClientHandler che gestisce il client specifico e viene passato ad un ThreadPoolExecutor con LinkedBlockingQueue. Se la ThreadPool ha raggiunto la sua MaxCoreSize, il task viene messo in coda.

Il server è anche responsabile della gestione dei dati da persistere degli utenti in formato JSON con l'ausilio della libreria Gson.
Per gestire l’accesso concorrente ai dati e per garantirne la consistenza, l’integrità e la sicurezza, i metodi che apportano modifiche al database sono synchronized.

## WQClient
L'utente può interagire con WQ mediante uno dei due client implementati.

WQClientGUI utilizza una semplice interfaccia grafica, mentre WQClient usa una interfaccia a linea di comando. Entrambi definiscono un insieme di comandi, presentati in un menu.

Le due versioni hanno una struttura simile. Sebbene con alcune differenze, entrambi hanno un thread per la ricezione delle richieste di sfida, la classe *Connection* per la connessione con il WQServer, l'interfaccia per il servizio di registrazione e la classe *Message* che modellizza i messaggi scambiati tra client e server.

### WQClient
Client implementato con un'interfaccia a linea di comando (CLI).
Questa implementazione è parzialmente operativa. Il servizio di sfida deve essere ancora sottoposto a test.

### WQClientGUI
Client implementato con un'interfaccia grafica realizzata con il framework [JavaFX](https://openjfx.io) e il software [SceneBuilder](https://gluonhq.com/products/scene-builder/) per la creazione di file FXML.

Schermata per il Login 
![image.png](./Screenshot/image.png)

Schermata introduttiva e menù
![image.png](./Screenshot/image1.png)
