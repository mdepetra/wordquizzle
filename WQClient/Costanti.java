/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Questo file contiene tutti i valori costanti usati nel WQClient
public class Costanti {
	// Argomenti per la connessione al server
	public static int portRMI = 6789;
	public static int portTCP = 6790;
	public static String hostnameServer = "localhost";
	public static int BUF_SZ = 512;				// Dimensione degli array di byte
	
	// Codici per i messaggi operazione dal client e per il menù
	public static final String REGISTRA_UTENTE = "registra_utente";
	public static final String LOGIN = "login";
	public static final String LOGOUT = "logout";
	public static final String AGGIUNGI_AMICO = "aggiungi_amico";
	public static final String LISTA_AMICI = "lista_amici";
	public static final String SFIDA = "sfida";
	public static final String MOSTRA_PUNTEGGIO = "mostra_punteggio";
	public static final String MOSTRA_CLASSIFICA = "mostra_classifica";
	public static final String QUIT = "quit";
	
	// Codici per i messaggi di risposta dal server
	public static String okCode = "200";
	public static String errorCode = "401";
	public static String challengeRequestCode = "302";
	public static String challengeAcceptCode = "303";
	public static String challengeDeclineCode = "304";
	public static String readyCode = "305";
	public static String nextWordCode = "306";
	public static String endWordCode = "307";
	public static String sfidaResultCode = "308";
	
	// Key per attributi
	public static String NICKUTENTE = "nickUtente";
	public static String PASSWORD = "password";
	public static String NICKAMICO = "nickAmico";
	public static String PUNTEGGIO = "punteggio";
	public static String SFIDEGIOCATE = "sfideGiocate";
	public static String CORRETTE = "paroleCorrette";
	public static String ERRATE = "paroleErrate";
	public static String NONFORNITE = "paroleNonFornite";
	public static String PAROLA = "parola";
	public static String TEMPORISPOSTA = "T1";
	public static String TEMPOSFIDA = "T2";
	public static String PUNTIAVVERSARIO = "puntiAvversario";
	public static String EXTRA = "extra";
	
	// Costruttore
	private Costanti() {
        // restrict instantiation
	}
	
}
