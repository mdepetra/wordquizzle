import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */


public class WQClient {
	private static Scanner stdin = null;
	private static Connection connection;
	private static boolean challengeMode;
    
	public static void main(String[] args) throws IOException {
		welcomeMessage();
		
		// Creazione dello Scanner per leggere dalla console
		stdin = new Scanner(System.in);
		connection = new Connection();
		
		boolean quit = false;
		challengeMode = false;
		
		// Ciclo fino a quando status non diventa false
		while(!quit) {
			
			if (!connection.getRequest() && !challengeMode) {
				// Stampa del menù
				menu();
	
				// Lettura del codice operazione
				// Elaborazione della stringa per estrarre
				String inputLine = stdin.nextLine();
				String[] parts = inputLine.split(" ");
		        String idOp = parts[0];
		        
				// Selezione dell'operazione da effettuare
				switch (idOp) {
					case Costanti.REGISTRA_UTENTE:
						registrazione(parts);
						break;
					case Costanti.LOGIN:
						login(parts);
						break;
					case Costanti.AGGIUNGI_AMICO:
						aggiungi_amico(parts);
						break;
					case Costanti.LISTA_AMICI:
						lista_amici();
						break;
					case Costanti.SFIDA:
						System.out.println("Client non configurato per il servizio di sfida.");
						//sfida(parts);
						break;
					case Costanti.MOSTRA_PUNTEGGIO:
						mostra_punteggio();
						break;
					case Costanti.MOSTRA_CLASSIFICA:
						mostra_classifica(parts);
						break;
					case Costanti.LOGOUT:
						logout();
						break;
					case Costanti.QUIT:
						System.out.println("Terminazione in corso");
						logout();
						quit = true;
						break;
					default:
						// Caso di default se il codice operazione non è presente
						System.out.println("Comando non riconosciuto.");
						break;
				}
			} else {
				if (challengeMode) {
					// Svolgimento sfida
					System.out.println("Via alla sfida di traduzione!");
					do {
						Message msg = connection.nextWord();
						
				    	if (msg.getCode().equals(Costanti.nextWordCode)) {
				    		// Parola da tradurre
				    		HashMap<String, String> attr = msg.getAttributi();
				    		System.out.println(msg.getMessageBody() + ": " + attr.get(Costanti.PAROLA));
				    		String inputLine = stdin.nextLine();
				    		// Creazione del messaggio di richiesta
				        	msg = new Message(Costanti.nextWordCode, inputLine, null);
				    		connection.sendOnChallengeSocket(msg);
				    	} else if (msg.getCode().equals(Costanti.endWordCode)) {
				    		// Parole Terminate
				    		String resp = msg.getMessageBody();
				    		System.out.println(resp);
				    		System.out.println("Per verificare il risultato, premere un qualsiasi tasto.");
				    		stdin.nextLine();
				    		// Creazione del messaggio di richiesta
				        	msg = new Message(Costanti.endWordCode, "Risultato?", null);
				    		connection.sendOnChallengeSocket(msg);
				    	} else if (msg.getCode().equals(Costanti.sfidaResultCode)) {
				    		// Esito della sfida e sua elaborazione
				    		HashMap<String, String> attr = msg.getAttributi();
				    		String risultato = "";
				    		risultato += "Hai tradotto correttamente " + attr.get(Costanti.CORRETTE) + " parole, ne hai sbagliate " + attr.get(Costanti.ERRATE) + " e non risposto " + attr.get(Costanti.NONFORNITE) + ".\n";
				    		risultato += "Hai totalizzato " + attr.get(Costanti.PUNTEGGIO) + " punti.\n";
				    		risultato += "Il tuo avversario ha totalizzato " + attr.get(Costanti.PUNTIAVVERSARIO) + " punti.\n";
				    		risultato += msg.getMessageBody();
				    		
				    		if (attr.get(Costanti.EXTRA) != null) {
								risultato += " Hai guadagnato " + attr.get(Costanti.EXTRA) + " punti extra, per un totale di " + (Integer.parseInt(attr.get(Costanti.PUNTEGGIO)) + Integer.parseInt(attr.get(Costanti.EXTRA))) + " punti!";
							}
				    		challengeMode = false;
				    		connection.endSfida();
				    		System.out.println(risultato);
				    	}
					} while (challengeMode);
					
				} else {
					// Gestione risposta alla richiesta di sfida
					System.out.println(connection.getRequestMsg());
					
					String inputLine = stdin.nextLine();
					
					Message msg;
					if (inputLine.equals("S")) {
						msg = new Message(Costanti.challengeAcceptCode, "Sfida accettata", null);
						challengeMode = true;
					} else {
						msg = new Message(Costanti.challengeDeclineCode, "Sfida rifiutata", null);
					}
					
			    	// Invio della risposta
			    	connection.sendResponse(msg);
					
			    	if (challengeMode) {
						Message rispostaSfida = connection.setupSfida();
						
						if (!rispostaSfida.getCode().equals(Costanti.okCode)) {
							System.out.println(rispostaSfida.getMessageBody());
						}
			    	}
				}
			}
		}
		stdin.close();
	}

	private static void welcomeMessage() {
		System.out.println("______________________ WELCOME TO ______________________\n");
		
		System.out.println("        WW           WW   OOOO   RRRRR   DDDDD           ");
		System.out.println("         WW         WW   OO  OO  RR  RR  DD  DD          ");
		System.out.println("          WW   W   WW    OO  OO  RRRRR   DD  DD          ");
		System.out.println("           WW WWW WW     OO  OO  RR RR   DD  DD          ");
		System.out.println("            WWW WWW       OOOO   RR  RR  DDDDD           \n");
		            
		System.out.println("  QQQQ   UU  UU  IIII  ZZZZZZZ  ZZZZZZZ  LL      EEEEEE  ");
		System.out.println(" QQ  QQ  UU  UU   II       ZZ       ZZ   LL      EE      ");
		System.out.println(" QQ   Q  UU  UU   II     ZZZ      ZZZ    LL      EEEEE   ");
		System.out.println(" QQ Q Q  UU  UU   II    ZZ       ZZ      LL      EE      ");
		System.out.println("  QQQQ Q  UUUU   IIII  ZZZZZZZ  ZZZZZZZ  LLLLLL  EEEEEE  ");
	/*	System.out.println("        __        __  ____    ____   ____");
		System.out.println("        \\ \\      / / / __ \\  | .. \\ |  _ \\ 	");
		System.out.println("         \\ \\    / / | |  | | | :: / | | | |");
		System.out.println("          \\ \\/\\/ /  | |__| | |   \\  | |_| |");
		System.out.println("           \\_/\\_/    \\____/  |_|\\_\\ |____/");
		System.out.println("  ____    _    _   _   ____   ____   _       _____");
		System.out.println(" / __ \\  | |  | | | | |__  | |__  | | |     |  ___|  ");
		System.out.println("| |  | | | |  | | | |   / /    / /  | |     | |_    ");
		System.out.println("| |__| | | |__| | | |  / /_   / /_  | |___  | |___   ");
		System.out.println(" \\___\\_\\  \\____/  |_| |____| |____| |_____| |_____|");
		System.out.println();
		*/
	}
	
	private static void menu() {
		System.out.println("\nCommandi: \n");
		if (!connection.getLogStatus()) {
			System.out.println("registra_utente <nickUtente > <password > registra l' utente");
			System.out.println("login <nickUtente > <password > effettua il login");
			System.out.println("quit chiude il programma\n");
		}
		else {
			System.out.println("aggiungi_amico <nickAmico> crea relazione di amicizia con nickAmico");
			System.out.println("lista_amici mostra la lista dei propri amici");
			System.out.println("sfida <nickAmico> richiesta di una sfida a nickAmico");
			System.out.println("mostra_punteggio mostra il punteggio dell’utente");
			System.out.println("mostra_classifica mostra una classifica degli amici dell’utente (incluso l’utente stesso)");
			System.out.println("logout effettua il logout\n");
		}
	}
	
	private static void registrazione(String[] parts) {
		if (checkArgs(parts.length, 3)) {
	        // Registrazione e recupero dell'esito
	 		Message response = connection.registrazione(parts[1], parts[2]);
	 		
	 		//System.out.println(response.getMessageBody());
		} else {
			// Usage Message
			System.out.println("Use: registra_utente <nickUtente> <password>");
		}
	}

	private static void login(String[] parts) {
		if (checkArgs(parts.length, 3)) {
			Message response = connection.login(parts[1], parts[2]);
	        
			System.out.println(response.getMessageBody());
		} else {
			// Usage Message
			System.out.println("Use: login <nickUtente> <password>");
		}
	}
	
	private static boolean checkArgs(int length, int i) {
		return (length >= i);
	}

	private static void logout() {
		Message response = connection.logout();
        
		System.out.println(response.getMessageBody());
	
	}
	
	private static void aggiungi_amico(String[] parts) {
		if (checkArgs(parts.length, 2)) {
			Message msg = connection.aggiungi_amico(parts[1]);
			System.out.println(msg.getMessageBody());
		}
		else {
			// Usage Message
			System.out.println("Use: aggiungi_amico <nickAmico>");
		}
	}
	
	private static void lista_amici() {
		Message msg = connection.lista_amici();
    	System.out.println(msg.getMessageBody());
	}
	
	private static void sfida(String[] parts) {
		if (checkArgs(parts.length, 2)) {
			// Invio della richiesta di sfida
			Message response = connection.sfida(parts[1]);
			
			System.out.println(response.getMessageBody());
			
			if (response.getCode().equals(Costanti.okCode)) {
				// La richiesta di sfida è stata presa in carico e recapita all'amico
				
				// Si attende la risposta dell'amico
				Message rispostaSfida = connection.setupSfida();
				
				if (rispostaSfida.getCode().equals(Costanti.okCode)) {
					challengeMode = true;
		    	} else {
		    		System.out.println(rispostaSfida.getMessageBody());
		    	}
			}
		}
		else {
			// Usage Message
			System.out.println("Use: sfida <nickAmico>");
		}
	}

	private static void mostra_punteggio() {
		Message msg = connection.mostra_punteggio();
    	
		if (msg.getCode().equals(Costanti.okCode)) {
			HashMap<String, String> attr = msg.getAttributi();
			// Creazione della stringa
			String resp = "";
			resp += Costanti.PUNTEGGIO + ": " + attr.get(Costanti.PUNTEGGIO) + "\n\t\t";
			resp += Costanti.SFIDEGIOCATE + ": " + attr.get(Costanti.SFIDEGIOCATE) + "\n\t\t";
			resp += Costanti.CORRETTE + ": " + attr.get(Costanti.CORRETTE) + "\n\t\t";
			resp += Costanti.ERRATE + ": " + attr.get(Costanti.ERRATE) + "\n\t\t";
			resp += Costanti.NONFORNITE + ": " + attr.get(Costanti.NONFORNITE);
			
			System.out.println(resp);
		}
	}
	
	private static void mostra_classifica(String[] parts) {
		JSONArray myList = connection.mostra_classifica();
	    
		System.out.println("Classifica: ");
		
		for (Object obj : myList) {
			JSONObject user = (JSONObject) obj;
			
			// Estrazione della causale e incremento dei dati
			String nome = (String) user.get(Costanti.NICKAMICO);
			int punteggio = ((Long)user.get(Costanti.PUNTEGGIO)).intValue();
			System.out.println("\t" + nome + "\t" + punteggio);
		}
	}
}