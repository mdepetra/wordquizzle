import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza la connessione con il server
public class Connection {
	private static Socket socketTCP;				// Socket TCP
	private static Thread threadChallengeRequest;	// Thread per le richieste di sfida
	private static BufferedWriter writer;
	private static BufferedReader reader;
	private static SocketChannel clientSfida;		// SocketChannel per la sfida
	private String nickUtente;						// Nome dell'utente
	private RegistrationService serverObject;		// Per la registrazione mediante RMI
	private DatagramSocket udpSocket;				// Per la socket UDP
	private boolean loggedIn;
	private boolean request;
	private String requestMsg;
	private InetAddress indResp;
	private int portResp;
	
	// Costruttore
	public Connection() {
		socketTCP = null;
		writer = null;
		reader = null;
		setLoggedOut();
		try {
			Registry r = LocateRegistry.getRegistry(Costanti.portRMI);
			Remote remoteObject = r.lookup("WQ-REGSERVER");
			serverObject = (RegistrationService) remoteObject;
		} catch (Exception e) {
			System.err.println("WQClient | Error in invoking object method " + e.toString());
		}
	}

	// Metodo per l'apertura di una socket TCP verso il server
	private void setupSocketTCP() {
		try {
			socketTCP = new Socket(Costanti.hostnameServer,Costanti.portTCP);
		} catch (UnknownHostException e) {
			System.err.println("WQClient | Errore nella creazione della socket TCP" + e.toString());
		} catch (IOException e) {
			System.err.println("WQClient | Errore nella creazione della socket TCP" + e.toString());
		}
	}
	
	// Metodo per la chiusura di una socket TCP
	private void closeSocketTCP() {
		try {
			if (socketTCP != null) {
				socketTCP.close();
				socketTCP = null;
			}
			
			if (writer != null)
				writer.close();
			
			if (reader != null)
				reader.close();
			
		} catch (IOException e) {
			System.err.println("WQClient | Errore nella chiusura della socket TCP" + e.toString());
		}
	}
	
	// Metodo per l'avvio di un thread che gestisce le richieste di sfida
	public void startUDP() {
		int port = socketTCP.getLocalPort();
		
		try {
			udpSocket = new DatagramSocket(port);
			// Creazione di un handler delle richieste di sfida
			ChallengeRequestHandler challengeRequestHandler = new ChallengeRequestHandler(udpSocket, this);
	        // Creazione del thread
			threadChallengeRequest = new Thread(challengeRequestHandler);
			// Avvio del thread
			threadChallengeRequest.start();
			// System.out.println("WQClientGUI | Per le richieste di sfida, Client pronto sulla porta " + port);
		} catch (SocketException e) {
			System.err.println("WQClientGUI | Errore nella creazione della Datagram Socket per le richieste di sfida.");
		}
	}
	
	// Metodo per la terminazione del thread che gestisce le richieste di sfida
	public void endUDP() {
		if (udpSocket != null)
			udpSocket.close();
	}
	
	// Metodo che invia le richieste 
	private void sendRequest(String request) {
		try {
			writer = new BufferedWriter(new OutputStreamWriter(socketTCP.getOutputStream()));
			writer.write(request);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			System.err.println("WQClient | Errore nella scrittura sulla socket TCP" + e.toString());
		}
	}
	
	// Metodo che riceve le risposte
	private Message receiveResponse() {
		Message msg = new Message();
		
        try {
        	reader = new BufferedReader(new InputStreamReader(socketTCP.getInputStream()));
			String response = reader.readLine();
			msg = msg.messageFromString(response);
		} catch (IOException e) {
			System.err.println("WQClient | Errore nella ricezione sulla socket TCP" + e.toString());
		}
        
		return msg;
	}
	
	// Metodo per la registrazione al servizio
	public Message registrazione(String nickUtente, String password) {
		Message msg = new Message();
		
		try {
			// Ricezione del messaggio di risposta
			String response = serverObject.registra_utente(nickUtente, password);
			msg = new Message().messageFromString(response);
			
			System.out.println("WQClient | " + msg.getMessageBody());
		} catch (RemoteException e) {
			System.err.println("WQClient | Error in invoking object method " + e.toString());
		}
		
		return msg;
	}
	
	// Metodo per l'autenticazione al servizio
	public Message login(String nickUtente, String password) {
		setupSocketTCP();
		
        // Elaborazione degli attributi da allegare al messaggio di richiesta
 		HashMap<String, String> attributi = new HashMap<String, String>();
 		attributi.put(Costanti.NICKUTENTE, nickUtente);
 		attributi.put(Costanti.PASSWORD, password);
 		
 		// Creazione del messaggio di richiesta
 		Message msg = new Message(Costanti.LOGIN, Costanti.LOGIN, attributi);
 		
 		// Invio del messaggio di richiesta
 		sendRequest(msg.toString());
 		
 		// Ricezione del messaggio di risposta
 		Message response = receiveResponse();
 		
 		if (response.getCode().equals(Costanti.errorCode))
 			closeSocketTCP();
 		else if (response.getCode().equals(Costanti.okCode)) {
 			setLoggedIn();
			// Se il login ha avuto esito positivo, si passa alla vista con il menù
			setNickUtente(nickUtente);
			// Avvio di un thread per gestire le richieste di sfida
			startUDP();
		}
        
 		return response;
	}
	
	// Metodo per il logout dal servizio
	public Message logout() {
		Message response = new Message();
		if (socketTCP != null) {
			// Creazione del messaggio di richiesta
			Message msg = new Message(Costanti.LOGOUT, Costanti.LOGOUT, null);
			
			// Invio del messaggio di richiesta
			sendRequest(msg.toString());
	
			// Ricezione del messaggio di risposta
			response = receiveResponse();
			
			setLoggedOut();
			closeSocketTCP();
			endUDP();
			endSfida();
			setNickUtente("");
		}
		return response;
	}
	
	// Metodo per l'aggiunta di un amico
	public Message aggiungi_amico(String nickAmico) {
        // Elaborazione degli attributi da allegare al messaggio di richiesta
		HashMap<String, String> attributi = new HashMap<String, String>();
		attributi.put(Costanti.NICKAMICO, nickAmico);
		
		// Creazione del messaggio di richiesta
		Message msg = new Message(Costanti.AGGIUNGI_AMICO, Costanti.AGGIUNGI_AMICO, attributi);
		
		// Invio del messaggio di richiesta
		sendRequest(msg.toString());

		// Ricezione del messaggio di risposta
		Message response = receiveResponse();
		
		return response;
	}
	
	// Metodo per la richiesta della lista di amici
	public Message lista_amici() {
		// Creazione del messaggio di richiesta
		Message msg = new Message(Costanti.LISTA_AMICI, Costanti.LISTA_AMICI, null);
		
		// Invio del messaggio di richiesta
		sendRequest(msg.toString());
		
		// Ricezione del messaggio di risposta
		Message response = receiveResponse();
        
		return response;
	}
	
	// Metodo per la lanciare una sfida
	public Message sfida(String nickAmico) {
		// Elaborazione degli attributi da allegare al messaggio di richiesta
		HashMap<String, String> attributi = new HashMap<String, String>();
		attributi.put(Costanti.NICKAMICO, nickAmico);
		
		// Creazione del messaggio di richiesta
		Message msg = new Message(Costanti.SFIDA, nickAmico, attributi);
		
		// Invio del messaggio di richiesta
		sendRequest(msg.toString());

		// Ricezione del messaggio di risposta
		Message response = receiveResponse();
		
		return response;
	}
	
	// Metodo per richiedere la porta sul quale connettersi per la sfida
	public Message setupSfida() {
		// Ricezione del messaggio della porta
		Message msg = receiveResponse();
		
		if (msg.getCode().equals(Costanti.okCode)) {
			// Elaborazione degli attributi ricevuti in allegato al messaggio di risposta
			HashMap<String, String> attr = msg.getAttributi();
			
			SocketAddress address = new InetSocketAddress(Costanti.hostnameServer, Integer.parseInt(attr.get("port")));
			try {
				clientSfida = SocketChannel.open(address);
				
				// Creazione del messaggio che notifica che l'utente è pronto per iniziare la sfida
				Message response = new Message (Costanti.readyCode, nickUtente, null);
				
				ByteBuffer buffer = ByteBuffer.wrap(response.toString().getBytes());
				
				// Invio del messaggio al server
				clientSfida.write(buffer);
				
	            buffer.flip();
			} catch (IOException e) {
				System.err.println("WQClient | Errore nel setup della sfida " + e.toString());
			}
		}
		
		return msg;
	}
	
	// Metodo per richiedere la parola successiva 
	public Message nextWord() {
		ByteBuffer buffer = ByteBuffer.allocate(Costanti.BUF_SZ);
    	boolean stop = false;
        Message msg = new Message();
        String response = "";
        
        try {
	        while (!stop) {
	        	buffer.clear();
	        	int bytesRead = clientSfida.read(buffer);
				
	        	buffer.flip();
	            response += StandardCharsets.UTF_8.decode(buffer).toString();
	            
	        	buffer.flip();
	        	
	    		if (bytesRead < Costanti.BUF_SZ)
	        		stop=true;
	        }
	        buffer.flip();
	        
	        // Elaborazione del messaggio ricevuto
	        msg = new Message().messageFromString(response);
        } catch (IOException e) {
        	System.err.println("WQClient | Errore nella ricezione dei messaggi per la sfida " + e.toString());
		}
        return msg;
	}
	
	// Metodo per inviare il messaggio sul socketChannel della sfida
	public void sendOnChallengeSocket(Message msg) {
		ByteBuffer buffer = ByteBuffer.wrap(msg.toString().getBytes());
    	while (buffer.hasRemaining()) {
    		try {
    			clientSfida.write(buffer);
			} catch (IOException e) {
				System.err.println("WQClient | Errore nella ricezione dei messaggi per la sfida " + e.toString());
			}
    	}
    	buffer.clear();
    	buffer.flip();
	}
	
	// Metodo per chiudere il socketChannel per la sfida
	public void endSfida() {
		if (clientSfida != null) 
			try {
				clientSfida.close();
				clientSfida = null;
			} catch (IOException e) {
				System.err.println("WQClient | Errore nella chiusura del socketChannel " + e.toString());
			}
	}
	
	// Metodo per recuperare il punteggio
	public Message mostra_punteggio() {
		// Creazione del messaggio di richiesta
		Message msg = new Message(Costanti.MOSTRA_PUNTEGGIO, Costanti.MOSTRA_PUNTEGGIO, null);
		
		// Invio del messaggio di richiesta
		sendRequest(msg.toString());

		// Ricezione del messaggio di risposta
		Message response = receiveResponse();
		
		return response;
	}
	
	// Metodo per recuperare la classifica
	public JSONArray mostra_classifica() {
		JSONArray myList = new JSONArray();
		try {
			// Creazione del messaggio di richiesta
			Message msg = new Message(Costanti.MOSTRA_CLASSIFICA, Costanti.MOSTRA_CLASSIFICA, null);
			
			// Invio del messaggio di richiesta
			sendRequest(msg.toString());
			
			// Ricezione del messaggio di risposta
			Message response = receiveResponse();
	        
			// Selezione la risposta da dare
			if (response.getCode().equals(Costanti.okCode)) {
				JSONParser parser = new JSONParser(); 
				
				myList = (JSONArray) parser.parse(response.getMessageBody());
			}
			
		} catch (ParseException e) {
			System.err.println("WQClient | Errore nel parsing " + e.toString());
		}
		return myList;
	}

	// Metodo per recuperare il nickUtente
	public String getNickUtente() {
		return nickUtente;
	}

	// Metodo per impostare il nickUtente
	public void setNickUtente(String nickname) {
		// TODO Auto-generated method stub
		this.nickUtente = nickname;
	}
	
	// Metodo per rispondere alla richiesta di sfida
	public void sendResponse(Message msg) {
		byte[] sendData = new byte[Costanti.BUF_SZ];
		// Trasformazione della stringa in bytes
		sendData = msg.toString().getBytes();
		
		// Invio della risposta 
		DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length,indResp,portResp);
		
		try {
			udpSocket.send(sendPacket);
		} catch (IOException e) {
			System.err.println("WQClient | Errore nella risposta della richiesta di sfida " + e.toString());
		}
	}

	public boolean getRequest() {
		// TODO Auto-generated method stub
		return request;
	}
	
	public boolean getLogStatus() {
		return loggedIn;
	}
	
	private synchronized void setLoggedOut() {
		loggedIn = false;
	}
	
	private synchronized void setLoggedIn() {
		loggedIn = true;
	}
	
	private synchronized void setRequestOn() {
		request = true;
	}
	
	private synchronized void setRequestOff() {
		request = false;
	}

	public synchronized void setNotification(Message msg, InetAddress indResp, int portResp) {
		setRequestOn();
		
		requestMsg = msg.getMessageBody() + " (S/N)";
		this.indResp = indResp;
		this.portResp = portResp;
		
		// Avvio di un thread per gestire la chiusura a tempo della notifica
		Thread thread = new Thread(() -> {
            try {
                Thread.sleep(Integer.parseInt(msg.getAttributi().get(Costanti.TEMPORISPOSTA)));
                if (getRequest()) {
                	setRequestOff();
                }
            } catch (Exception exp) {
                exp.printStackTrace();
            }
        });
        thread.setDaemon(true);
        thread.start();
	}

	public synchronized String getRequestMsg() {
		setRequestOff();
		
		return requestMsg;
	}
}
