import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra, 549105
 *
 */

// Classe che modellizza il gestore delle richeiste di sfida
public class ChallengeRequestHandler implements Runnable {
	private DatagramSocket serverSocket;
	private InetAddress indResp;
	private int portResp;
	private Connection connection; 
	
	// Costruttore
	public ChallengeRequestHandler(DatagramSocket serverSocket, Connection con) {
		this.serverSocket = serverSocket;
		this.connection = con;
	}
	
	@Override
	public void run() {
		try {
			byte[] receiveData = new byte[Costanti.BUF_SZ];
			
			DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
			
			while(!Thread.currentThread().isInterrupted()) {
				// Ricezione di un paccketto dal client
				serverSocket.receive(receivePacket);
				
				// Trasformazione del campo DATA in una stringa
				String bytesToString = new String(receivePacket.getData(), 0, receivePacket.getLength(), "US-ASCII");
				
				// Elaborazione del messaggio ricevuto
				Message msg = new Message().messageFromString(bytesToString);
				
				// Configurazione della connessione in corso
				if (msg.getCode().equals(Costanti.challengeRequestCode)) {
					// C'è una richiesta di sfida
					indResp = receivePacket.getAddress();
					portResp = receivePacket.getPort();
						
					connection.setNotification(msg, indResp, portResp);
					
				}
			}
		} catch (IOException e) {
			System.err.println("WQClient | Errore " + e.toString());
		} finally {
			if (serverSocket != null)
				serverSocket.close();
		}
	}
	
	
}
