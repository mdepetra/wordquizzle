import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza il gestore di una sfida
public class ChallengeHandler implements Runnable {
	private String nickUtente;	// Nome dell'utente che ha lanciato la sfida (SFIDANTE)
	private String nickAmico;	// Nome dell'utente che ha ricevuto la sfida (SFIDATO)
    private static String fileName = Costanti.dizionarioName; // Nome del file in cui è memorizzato il dizionario
	private Database db;		// Database
	private List<String> dizionario;	// Lista per memorizzare il dizionario
	private Thread sfidaTimerThread;	// Thread per eseguire il task tempo a disposizione per la sfida
	private List<String> paroleScelte;	// Lista delle parole scelte
	private List<String> paroleTradotte;// Lista delle parole scelte tradotte
	private ChallengeInfo statU;		// Informazioni dell'utente nickUtente per la sfida
	private ChallengeInfo statA;		// Informazioni dell'utente nickUtente per la sfida
	private volatile AtomicInteger risultatiInviati; // Quando sono stati inviati entrambi i risultati, si termina

	// Costruttore
	public ChallengeHandler(String nickUtente, String nickAmico, Database db) {
		this.nickUtente = nickUtente;
		this.nickAmico = nickAmico;
		this.db = db;
		risultatiInviati = new AtomicInteger(0);
	}
	
	@Override
	public void run() {
		// Invia richiesta di sfida a nickAmico con UDP
		Socket socketA = this.db.getClientSocket(nickAmico);
		Socket socketU = this.db.getClientSocket(nickUtente);
		
		// Imposta lo status di entrambi gli utenti a occupato
		this.db.setBusy(nickUtente);
		this.db.setBusy(nickAmico);
		
		// Inoltro della richiesta di sfida
		Message resp = sendUDPChallenge(nickUtente, Costanti.T1, socketA);
		
		// Ottiene una risposta entro T1
		if (!resp.getCode().equals(Costanti.challengeAcceptCode)) {
			// Risposta negativa
			// Si avvisa nickUtente del rifiuto della sfida
			sendResponse(resp.toString(), socketU);
			// Si liberano gli utenti
			this.db.setFree(nickUtente);
			this.db.setFree(nickAmico);
		} else {
			// Risposta positiva
			
			// Recupero del dizionario
			retrieveDictionary();
			
			// Scelta e traduzione delle K parole della sfida
			try {
				chooseAndTranslate(socketU, socketA);
			} catch (IOException e) {
				System.err.println("WQServer | Sfida con " + nickAmico + " : errore nella traduzione " + e.toString());
				// Servizio di sfida Off
				this.db.setOffSfidaService();
				// Si liberano gli utenti
				this.db.setFree(nickUtente);
				this.db.setFree(nickAmico);
				// Si avvisano gli utenti dell'errore nel servizio di sfida
				sendResponse((new Message(Costanti.challengeDeclineCode, "Servizio di sfida non disponibile", null)).toString(), socketU);
				sendResponse((new Message(Costanti.challengeDeclineCode, "Servizio di sfida non disponibile", null)).toString(), socketA);
			}
		    
		    // Tutte le parole sono state tradotte
			if (paroleTradotte.size() == Costanti.noParoleK) {
				int port = -1;
				sfidaTimerThread = null;
				
				ServerSocketChannel serverChannel;
				Selector selector;
				try {
					// Creazione del SocketChannel
					serverChannel = ServerSocketChannel.open();
					ServerSocket ss = serverChannel.socket(); 
					
					// A port number of zero will let the system pick up an ephemeral port in a bind operation.
					InetSocketAddress address = new InetSocketAddress(0); 
					ss.bind(address); 
					port = ss.getLocalPort();
					
					// Il server è in ascolto
					System.out.println("WQServer | In ascolto sulla porta " + port + " per la sfida " + nickUtente + "-" + nickAmico);
					
					// Channel in modalità non bloccante
					serverChannel.configureBlocking(false);
					
					// Creazione del selector
					selector = Selector.open();
					
					// Si registra il server al selector
					serverChannel.register(selector, SelectionKey.OP_ACCEPT);
				} catch (IOException e) { 
					System.err.println("WQServer | Errore: " + e.toString());
					return;
				}
				
				if (port != -1) {
					// Comunicazione della porta a entrambi gli sfidanti
					HashMap <String, String> attr = new HashMap<String, String>();
					attr.put("port", Integer.toString(port));
					Message msgPort = new Message(Costanti.okCode, "Porta per la sfida", attr);
					
					// Invio della porta ai due utenti
					sendResponse(msgPort.toString(), socketU);
					sendResponse(msgPort.toString(), socketA);
					
					// Inizializzazione delle informazioni sulla sfida
					statU = statA = new ChallengeInfo();
					
					// Fino a quando non ho inviato i risultati a entrambi gli utenti
					while (risultatiInviati.get() != 2) { 
						// Seleziona i canali pronti per almeno una delle operazioni di I/O tra quelli registrati con quel selettore
						try {
							selector.select();
						} catch (IOException ex) {
							System.err.println("WQServer | Errore: " + ex.toString());
							break;
						}
						
						Set <SelectionKey> readyKeys = selector.selectedKeys();
						Iterator <SelectionKey> iterator = readyKeys.iterator();
		
						while (iterator.hasNext()) {
							SelectionKey key = iterator.next();
							
							// Rimuove la chiave dal Selected Set, ma non dal registered Set 
							iterator.remove();
							
							try {
								if (key.isAcceptable()) {
									// A connection was accepted by a ServerSocketChannel
									ServerSocketChannel server = (ServerSocketChannel) key.channel(); 
									SocketChannel client = server.accept();
									
									System.out.println("WQServer | Connessione accettata da " + client);
									
									// Channel in modalità Non Blocking
									client.configureBlocking(false);
	
									// Registrazione di un canale su un selettore e impostazione dell'interest set per le operazioni di lettura
									client.register(selector, SelectionKey.OP_READ, new ChallengeInfo());
									
									// Alla prima connessione creo il thread timer per la sfida (almeno un utente ha iniziato a giocare)
									if (sfidaTimerThread == null) {
										System.out.println("WQServer | Timer Sfida avviato");
										SfidaTimer sfidaTimer = new SfidaTimer(); 
										sfidaTimerThread = new Thread(sfidaTimer);
										sfidaTimerThread.start();
									}
								} else if (key.isReadable()) {
									// A channel is ready for reading
									read(key);
								} else if (key.isWritable()) { 
									// A channel is ready for writing
									write(key);
								} 
							} catch (IOException ex) { 
								key.cancel();
								try { 
									key.channel().close();
								} catch (IOException e) {
									System.err.println("WQServer | Errore nella key.channel().close() : " + e.toString());
								}
							}
						}
					}
					
					// Se il thread per il timer della sfida è ancora vivo, si interrompe
					if (sfidaTimerThread.isAlive()) {
						sfidaTimerThread.interrupt();
					}
					
				    // La sfida è terminata, si aggiornano i punteggi e si liberano gli utenti
					this.db.updatePunteggio(statU);
					this.db.updatePunteggio(statA);
				    this.db.setFree(nickUtente);
					this.db.setFree(nickAmico);
					
					System.out.println("WQServer | Sfida " + nickUtente + "-" + nickAmico + " terminata");
				}
			}
		}
	}

	// Metodo per l'inoltro della richiesta via UDP
	public Message sendUDPChallenge(String nickSfidante, int t1, Socket socketA) {
		// Recupero delle informazioni per inoltrare la richiesta di sfida usando la comunicazione UDP
		InetSocketAddress iaUtente = ((InetSocketAddress) socketA.getRemoteSocketAddress());
		int portUDPA = iaUtente.getPort();
		String ipA = (iaUtente.getAddress()).toString().replace("/","");
		
		DatagramSocket dSocket = null;
		
		InetAddress IPAddress;
		Message msg = new Message();
		try {
			dSocket = new DatagramSocket();
			IPAddress = InetAddress.getByName(ipA);
			
			byte[] sendData = new byte[Costanti.BUF_SZ];
			byte[] receiveData = new byte[Costanti.BUF_SZ];
			
			// Preparazione del messaggio
			HashMap<String, String> attr = new HashMap<String, String>();
			attr.put(Costanti.NICKAMICO, nickSfidante);
			attr.put(Costanti.TEMPORISPOSTA, Integer.toString(Costanti.T1));
			
			String request = (new Message(Costanti.challengeRequestCode, "Ricevuta una richiesta di sfida da " + nickSfidante, attr)).toString();
				
			// Trasferimento in un array
			sendData = request.getBytes();
				
			// Invio del pacchetto
			DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length,IPAddress,portUDPA);
			dSocket.send(sendPacket);
			
			// Impostazione del timeout T1
			dSocket.setSoTimeout(t1);
			
			// Ricezione della risposta
			DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
			dSocket.receive(receivePacket);
			
			String response = new String(receivePacket.getData(), 0, receivePacket.getLength(), "US-ASCII");
			
			// Se viene ricevuta la risposta
			msg = new Message().messageFromString(response);
		} catch (SocketTimeoutException e) {
			// Se non viene ricevuta la risposta entro il timeout
			System.err.println("WQServer | Sfida con " + nickAmico + " : Timeout scaduto " + e.toString());
			return new Message(Costanti.challengeDeclineCode, "Errore per la sfida con " + nickAmico + ": Timeout scaduto", null);
		} catch (SocketException | UnknownHostException e) {
			System.err.println("WQServer | Sfida con " + nickAmico + " : " + e.toString());
			return new Message(Costanti.challengeDeclineCode, "Errore per la sfida con " + nickAmico, null);
		} catch (IOException e) {
			System.err.println("WQServer | Sfida con " + nickAmico + " : " + e.toString());
			return new Message(Costanti.challengeDeclineCode, "Errore per la sfida con " + nickAmico, null);
		} finally {
			// Chiusura della socket
			if (dSocket != null)
				dSocket.close();
		}
		return msg;
	} 
	
	// Metodo che recupera il file json
	private void retrieveDictionary() {
		// Creazione del nuovo file
        File f = new File(fileName); 
  
        if (!f.exists()) 
        	return;
        
		try {
			FileChannel inChannel =  FileChannel.open(Paths.get(fileName), StandardOpenOption.READ);
			String json = "";
			ByteBuffer buffer = ByteBuffer.allocate(1024*1024*100);
            boolean stop=false;
	
            // Lettura del file con il channel di input 
            while (!stop) {
				int bytesRead = inChannel.read(buffer);
				if (bytesRead == -1) stop = true;
				buffer.flip();
				while (buffer.hasRemaining())
					json += StandardCharsets.US_ASCII.decode(buffer).toString();
				buffer.clear();
			}
            
			inChannel.close();
			
			Gson gson = new Gson();
			Type type = new TypeToken<ArrayList<String>>(){}.getType();
			dizionario = gson.fromJson(json, type);
		} catch (IOException e) {
			System.err.println("WQServer | Sfida con " + nickAmico + " : errore nel recupero del dizionario " + e.toString());
		} 
	}
	
	// Metodo per la scelta e la traduzione delle parole
	private void chooseAndTranslate(Socket socketU, Socket socketA) throws IOException {
		// Scelta, in modo casuale, di K parole da un dizionario contenente N parole italiane 
		paroleScelte = new ArrayList<String>(Costanti.noParoleK);
		
	    for (int i=0; i<Costanti.noParoleK; i++) {
	        int randomIndex = (int)(Math.random()*dizionario.size());
	        paroleScelte.add(dizionario.get(randomIndex));
	        dizionario.remove(randomIndex);
	    }
	    
	    // Recupero di tutte le traduzioni
	    paroleTradotte = new ArrayList<String>();
	    
	    try {
	    	for (int i=0; i<Costanti.noParoleK; i++) {
				URL url = new URL(Costanti.pathName + paroleScelte.get(i) + Costanti.tag);
				HttpURLConnection uc = (HttpURLConnection) url.openConnection();
				
				if (uc.getResponseCode() == Integer.parseInt(Costanti.okCode)) {
					BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
	                String line = null;
					StringBuffer sb=new StringBuffer();
					while((line=in.readLine())!=null){
						sb.append(line);
					}
					
					JSONObject obj = (JSONObject) new JSONParser().parse(sb.toString()); 
					
					JSONObject responseData = (JSONObject) obj.get("responseData");
				    
				    paroleTradotte.add((String) responseData.get("translatedText"));
				    System.out.println("WQServer | Parola " + (i+1) + "/" + Costanti.noParoleK + " " + responseData.get("translatedText")); 
				} else {
					System.err.println("WQServer | Sfida con " + nickAmico + " : errore nel servizio di traduzione ");
					// Si avvisano gli utenti dell'errore nel servizio di sfida
					sendResponse((new Message(Costanti.challengeDeclineCode, "Servizio di sfida non disponibile", null)).toString(), socketU);
					sendResponse((new Message(Costanti.challengeDeclineCode, "Servizio di sfida non disponibile", null)).toString(), socketA);
					// Si liberano gli utenti
					this.db.setFree(nickUtente);
					this.db.setFree(nickAmico);
				}
				uc.disconnect();
	    	}
	    } catch (ParseException e) {
	    	System.err.println("Sfida con " + nickAmico + " : errore nel parsing del servizio di traduzione " + e.toString());
	    	// Si avvisano gli utenti dell'errore nel servizio di sfida
			sendResponse((new Message(Costanti.challengeDeclineCode, "Servizio di sfida non disponibile", null)).toString(), socketU);
			sendResponse((new Message(Costanti.challengeDeclineCode, "Servizio di sfida non disponibile", null)).toString(), socketA);
			// Si liberano gli utenti
			this.db.setFree(nickUtente);
			this.db.setFree(nickAmico);
		}
	}
	
	// Metodo per inviare messaggi su una socket passata come argomento
	public void sendResponse(String resp, Socket clientSocket) {
		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
			writer.write(resp);
			writer.newLine();
			writer.flush();
			System.out.println("WQServer | " + Thread.currentThread().getName() + " invia a " + nickUtente + ":\n\t\t" + resp); 
		} catch (IOException e) {
			System.err.println("WQServer | " + Thread.currentThread().getName() + " errore nell'invio a " + nickUtente);
		}
	}
	
	// Metodo per leggere dal client
	public void read (SelectionKey key) {
		try {
			SocketChannel client = (SocketChannel) key.channel();
			ByteBuffer input = ByteBuffer.allocate(Costanti.BUF_SZ);
			
			// Recupero dell'allegato
			ChallengeInfo stat = (ChallengeInfo) key.attachment();
			
			// Lettura del messaggio ricevuto dal client
			int bytesRead = client.read(input);
			
			if (bytesRead == -1) {
				// Se il client è terminato, si chiude il channel, si imposta il flag di fine (setEnded) e si salvano le informazioni di sfida
				System.out.println("WQServer | Connessione interrotta da " + client);
				key.cancel(); 
	            client.close();
	            // Impostazione del flag di fine
	            stat.setEnded();
	            // Incremento dei risultati inviati
	            risultatiInviati.incrementAndGet();
	            // Salvataggio delle informazioni di sfida
				saveStat(stat);
			} else {
				// Decodifica dei bytes letti
				input.flip();
				String messaggio = stat.getMsg();
				messaggio += StandardCharsets.UTF_8.decode(input).toString();
				// Salvataggio del messaggio letto
				stat.setMsg(messaggio);
				
				if (bytesRead < Costanti.BUF_SZ) {
					// Il server ha letto tutto dal buffer, si suppone che il messaggio sia stato completamente letto
					// Elaborazione del messaggio
			        Message msg = new Message().messageFromString(stat.getMsg());
			        if (msg.getCode().equals(Costanti.readyCode)) {
			        	// Prima connessione per riconoscere l'utente che si è unito alla sfida
			        	stat.setNickname(msg.getMessageBody());
			        } else if (msg.getCode().equals(Costanti.nextWordCode)) {
			        	// Ricevuta una nuova traduzione
			        	
			        	// Si valuta la traduzione
			        	int i = stat.getIndex();
			        	if (msg.getMessageBody().equals(paroleTradotte.get(i)))
			        		stat.incrCorrette();
			        	else if (msg.getMessageBody().equals(""))
			        		stat.incrInBianco();
			        	else
			        		stat.incrSbagliate();
			        	
			        	// Si incrementa l'indice della prossima parola da inviare
			        	stat.incrIndex();
			        }
			        
			        // Pulizia e reinizializzazione a stringa vuota del messaggio
					stat.setMsg("");
					
					// Modifica dell'interest set per le operazioni di scrittura
			        key.interestOps(SelectionKey.OP_WRITE);
				}
				
				// Si allega output alla SelectionKey
				key.attach(stat);
			}
			
		} catch (IOException e) {
			System.err.println("WQServer | Errore nella lettura dal client: " + e.toString());
		}
	}

	// Metodo per scrivere al client
	private void write (SelectionKey key) {
		// Non si verificano i byte scritti perchè per come è stato concepito il server le risposte inviate non saranno mai superiori a 512 byte
		try {
			SocketChannel client = (SocketChannel) key.channel();
			ChallengeInfo stat = (ChallengeInfo) key.attachment();
			
			// Si controlla se l'utente non ha terminato la sfida
			if (!stat.getEnded()) {
				// L'utente non ha terminato la sfida, si controlla che il timer della sfida
				if (sfidaTimerThread.isAlive()) {
					// Il timer è ancora attivo, l'utente ha tempo
					int i = stat.getIndex();
					Message msg;
					if (i<paroleScelte.size()) {
						// L'utente ha ancora delle parole da tradurre
						HashMap<String, String> attr = new HashMap<String, String>();
						attr.put(Costanti.PAROLA, paroleScelte.get(i));
						msg = new Message(Costanti.nextWordCode, "Challenge " + Integer.toString(i+1) + "/" + Integer.toString(paroleScelte.size()), attr);
					
						System.out.println("WQServer | " + stat.getNickname() + " spedita la parola " + (i+1));
					} else {
						// L'utente ha terminato le parole da tradurre
						msg = new Message(Costanti.endWordCode, "Hai esaurito le parole.\n\nATTENDI L'ESITO DELLA SFIDA", null);
						
						// L'utente ha terminato la sfida
						stat.setEnded();
						System.out.println("WQServer | " + stat.getNickname() + " ha esaurito le parole");
					}
					
					ByteBuffer output = ByteBuffer.wrap(msg.toString().getBytes());
					
					// Invio della risposta al client
					client.write(output);
					output.flip();
					
					// Modifica dell'interest set per le operazioni di lettura
			        key.interestOps(SelectionKey.OP_READ);
				} else {
					// Il tempo per la sfida è terminato
					System.out.println("WQServer | " + stat.getNickname() + " TIMEOUT SFIDA");
					stat.setEnded();
				}
				
				// Si salvano le informazioni della sfida di questo utente
				saveStat(stat);
				
				key.attach(stat);
			} else {
				// Se entrambi hanno terminato, si invia il risultato
				if (statU.getEnded() && statA.getEnded()) {
					// Entrambi hanno terminato
					Message msg = new Message();
					
					if (stat.getNickname().equals(nickUtente)) {
						msg = createMessage(stat, statA);
					} else if (stat.getNickname().equals(nickAmico)) {
						msg = createMessage(stat, statU);
					}
					
					ByteBuffer output = ByteBuffer.wrap(msg.toString().getBytes());
					
					// Invio della risposta al client
					client.write(output);
					output.flip();
					
					key.cancel();
					
					// Chusura del channel
					client.close();
					
					System.out.println("WQServer | " + stat.getNickname() + " spedito il risultato " + msg.getMessageBody());
				} else {
					// Se solo uno ha terminato e richiede nuovamente il risultato
					Message msg = new Message(Costanti.endWordCode, "Hai esaurito le parole.\n\nATTENDI L'ESITO DELLA SFIDA", null);
					
					ByteBuffer output = ByteBuffer.wrap(msg.toString().getBytes());
					
					// Invio della risposta al client
					client.write(output);
					output.flip();
					
					// L'utente ha terminato la sfida
					key.interestOps(SelectionKey.OP_READ);
					key.attach(stat);
				}
			}
		} catch (IOException e) {
			System.err.println("WQServer | Errore nella scrittura al client: " + e.toString());
		}
	}
	
	// Metodo per salvare le informazioni della sfida
	private void saveStat(ChallengeInfo stat) {
		if (stat.getNickname().equals(nickUtente)) {
			statU = stat;
		} else if (stat.getNickname().equals(nickAmico)) {
			statA = stat;
		}
	}

	// Metodo per creare il messaggio di esito della sfida
	private Message createMessage(ChallengeInfo stat, ChallengeInfo statAvversario) {
		int puntiUtente = stat.calcolaPunti();
		int puntiAvversario = statAvversario.calcolaPunti();
		
		// Elaborazione degli attributi del messaggio
		HashMap<String, String> attr = new HashMap<String, String>();
		attr.put(Costanti.PUNTEGGIO, Integer.toString(stat.calcolaPunti()));
		attr.put(Costanti.CORRETTE, Integer.toString(stat.getCorrette()));
		attr.put(Costanti.ERRATE, Integer.toString(stat.getSbagliate()));
		attr.put(Costanti.NONFORNITE, Integer.toString(stat.getInBianco()));
		attr.put(Costanti.PUNTIAVVERSARIO, Integer.toString(puntiAvversario));
		
		String body;
		// Si stabilisce il vincitore
		if (puntiUtente > puntiAvversario) {
			attr.put(Costanti.EXTRA, Integer.toString(Costanti.pExtraZ));
			body = "HAI VINTO";
			stat.setExtra();
		} else if (puntiUtente == puntiAvversario) {
			body = "PAREGGIO";
		} else {
			body = "GAME OVER";
			statAvversario.setExtra();
		}
		
		// Si crea il messaggio e si incrementa il numero di risultati inviati
		Message msg = new Message(Costanti.sfidaResultCode,body, attr);
		risultatiInviati.incrementAndGet();
		return msg;
	}

}
