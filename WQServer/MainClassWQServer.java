import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe Main del WQServer
public class MainClassWQServer {
	private static ServerSocket serverSocket;	// Socket per le connessioni TCP dei client
    private static ThreadPoolExecutor executor; // ThreadPoolExecutor per i task clienthandler
    
	public static void main(String[] args) {
		// Creazione di un'istanza di Database
		Database db = new Database();
		
		try {
			// Creazione di un'istanza dell'oggetto registrazioneService
			RegistrationServiceImpl registrazioneService = new RegistrationServiceImpl(db);
			
			// Esportazione dell'oggetto
			RegistrationService stub = (RegistrationService) UnicastRemoteObject.exportObject(registrazioneService, 0);
			
			// Creazione di un registry sulla porta portRMI
			LocateRegistry.createRegistry(Costanti.portRMI);
			Registry r = LocateRegistry.getRegistry(Costanti.portRMI);
			
			// Pubblicazione dello stub nel registry
			r.rebind("WQ-REGSERVER", stub);
			
			System.out.println("WQRegServer | Stub pubblicato nel registry alla porta " + Costanti.portRMI);
		} catch (RemoteException e) {
			System.err.println("WQRegServer | Errore RMI " + e.toString());
		}
		
		try {
			// Creazione della ServerSocket
			serverSocket = new ServerSocket(Costanti.portTCP);
			// Creazione della ThreadPool
			executor = new ThreadPoolExecutor(Costanti.nThreads/2, Costanti.nThreads, 5, TimeUnit.MINUTES, new LinkedBlockingQueue<Runnable>());
			
			System.out.println("WQServer | Pronto sulla porta " + Costanti.portTCP);
			
			// Avvio di un thread per controllare la disponibilità del servizio di traduzione
			Thread thread = new Thread(() -> {
	            
            	while (true) {
    				try {
    					// Si attende timerService millisecondi prima di verificare il servizio di sfida, 
    					// nel frattempo si assume che sia disponibile, in caso contrario sarà il ChallengeHandler 
    					// della sfida a notificare il disservizio
		                Thread.sleep(Costanti.timerService);
		                
		                // Connessione al servizio di traduzione
    					URL url = new URL(Costanti.pathName);
        				HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        				
	    				if (uc.getResponseCode() == Integer.parseInt(Costanti.okCode)) {
	    					System.out.println("WQServer | Servizio di traduzione disponibile");
	    					db.setOnSfidaService();
	    				} else {
	    					System.out.println("WQServer | Servizio di traduzione non disponibile");
	    	            	db.setOffSfidaService();
	    				}
	    				
	    				uc.disconnect();
    				} catch (Exception e) {
    					System.out.println("WQServer | Servizio di traduzione non disponibile");
    	            	db.setOffSfidaService();
    	            }
            	}
	        });
	        thread.setDaemon(true);
	        thread.start();
			
	        ClientHandler clientHandler;
	        
			while (true) {
				// Per ogni nuova connessione accettata, si crea un'istanza di ClientHandler 
	            clientHandler = new ClientHandler(serverSocket.accept(), db);
	            
	            // Esecuzione del task con il ThreadPoolExecutor
	            executor.execute(clientHandler);
	        }
		} catch (IOException e) {
			System.err.println("WQServer | Communication error " + e.toString());
		}

        
	}

}
