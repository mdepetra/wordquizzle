import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che implementa il servizio di registrazione
public class RegistrationServiceImpl extends RemoteServer implements RegistrationService {
	private static final long serialVersionUID = 9114425962502079101L;
	
	private Database db;
	
	public RegistrationServiceImpl(Database db) {
		this.db = db;
	}
	
	@Override
	public String registra_utente(String nickUtente, String password) throws RemoteException {
		return db.registraUtente(nickUtente, password);
	}

}
