import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.lang.reflect.Type;
import java.net.Socket;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza il database del sistema WQ
public class Database {
	private static final String fileName = Costanti.databaseName; // Nome del file su cui è memorizzato il database
	private HashMap<String, User> db;	// Struttura di supporto per la gestione dei dati.
	private boolean sfidaService;		// Booleano dello status del sfida (si basa sulla possibilitò di raggiungere il server
	
	// Costruzione del database
	public Database() {
		this.db = new HashMap<String, User>();
		setOnSfidaService();
		// Se già esistente, si recupera il database dal file 
		retrieveJSONFile();
	}
	
	// Metodo sysynchronized per la registrazione dell'utente
	public synchronized String registraUtente(String nickUtente, String password) {
		// Si controlla se nickUtente è già registrato
		if (this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, "Registrazione fallita: utente già registrato", null)).toString();
		
		// Si controlla se la password è vuota
		if (password == "")
			return (new Message(Costanti.errorCode, "Registrazione fallita: password vuota", null)).toString();
		
		// Si registra l'utente al servizio
		User u = new User(nickUtente, password, 0, 0, 0, 0, 0, new TreeSet<String>());
		this.db.put(nickUtente, u);
		
		// Si salva il db su file in formato JSON
		saveJSONFile();
		
		return (new Message(Costanti.okCode, "Registrazione eseguita con successo", null)).toString();
	}
	
	// Metodo sysynchronized per l'autenticazione dell'utente
	public synchronized String loginUtente(String nickUtente, String password, Socket clientSocket) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, "Login fallito: utente inesistente", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (u.isOnline())
			return (new Message(Costanti.errorCode, "Login fallito: utente già online", null)).toString();
		
		// Si verifica la validità dell'identità di nickUtente
		if(u.verificaID(password)) {
			u.setOnline(true);
			u.setBusy(false);
			u.setClientSocket(clientSocket);
			
			return (new Message(Costanti.okCode, "Login eseguito con successo", null)).toString();
		}
		
		return (new Message(Costanti.errorCode, "Login fallito: password errata", null)).toString();
	}

	// Metodo sysynchronized per il logout dell'utente
	public synchronized String logoutUtente(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, "Logout fallito: utente inesistente", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline())
			return (new Message(Costanti.errorCode, "Logout fallito: utente è offline", null)).toString();
		
		u.setOnline(false);
		u.setBusy(false);
		return (new Message(Costanti.okCode, "Logout eseguito con successo", null)).toString();
	}

	// Metodo sysynchronized per l'aggiunta di un amico
	public synchronized String aggiungiAmico(String nickUtente, String nickAmico) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, "Amicizia non creata: " + nickUtente + " non registrato", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline())
			return (new Message(Costanti.errorCode, "Amicizia non creata: " + nickUtente + " offline", null)).toString();
		
		// Si controlla che nickUtente e nickAmico siano diversi 
		if (nickUtente.equals(nickAmico))
			return (new Message(Costanti.errorCode, "Amicizia non creata: " + nickUtente + " sei già amico di te stesso", null)).toString();
		
		// Si controlla se nickAmico è registrato
		if (!this.db.containsKey(nickAmico))
			return (new Message(Costanti.errorCode, "Amicizia non creata: " + nickAmico + " non registrato", null)).toString();
		
		// Si controlla se l'amicizia è già esistente
		if (u.isAmico(nickAmico))
			return (new Message(Costanti.errorCode, "Amicizia " + nickUtente + "-" + nickAmico + " già esistente", null)).toString();

		// nickAmico è registrato e lo si recupera dal db
		User a = this.db.get(nickAmico);
		
		// Si aggiungono le amicizie reciproche
		boolean result = (u.aggiungiAmico(nickAmico) && a.aggiungiAmico(nickUtente));
		
		if (!result)
			return(new Message(Costanti.errorCode, "Amicizia non creata: errore interno", null)).toString();
		
		// Se sono state aggiunte entrambe le amicizie, si salva il db su file in formato JSON
		saveJSONFile();
		return (new Message(Costanti.okCode, "Amicizia " + nickUtente + "-" + nickAmico + " creata", null)).toString();
	}
	
	// Metodo per recuperare la lista degli amici dal database
	public synchronized String listaAmici(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, "Lista amici: " + nickUtente + " non registrato", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return (new Message(Costanti.errorCode, "Lista amici: " + nickUtente + " offline", null)).toString();
		
		// Creazione del JSONArray
		Set<String> lAmici = u.getAmici();
		Gson gson = new Gson();
		
		return (new Message(Costanti.okCode, gson.toJson(lAmici), null)).toString();
	}
	
	// Metodo per verificare la fattibilità di una sfida tra nickUtente e nickAmico
	public String sfida(String nickUtente, String nickAmico) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, nickUtente + " non registrato", null)).toString();
		
		// Si controlla se il servizio di sfida è disponibile
		if (!sfidaService)
			return (new Message(Costanti.errorCode, "Servizio di sfida non disponibile", null)).toString();
		
		// Si controlla se nickAmico è registrato al servizio
		if (!this.db.containsKey(nickAmico))
			return (new Message(Costanti.errorCode, nickAmico + " non registrato", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return (new Message(Costanti.errorCode, nickUtente + " offline", null)).toString();
		
		// Si controlla se nickUtente e nickAmico sono amici
		if (!u.isAmico(nickAmico))
			return (new Message(Costanti.errorCode, nickAmico + " non è fra gli amici", null)).toString();
		
		User a = this.db.get(nickAmico);

		// Si controlla se nickAmico è online
		if (!a.isOnline()) 
			return (new Message(Costanti.errorCode, nickAmico + " offline", null)).toString();
		
		// Si controlla se nickAmico è busy
		if (a.isBusy()) 
			return (new Message(Costanti.errorCode, nickAmico + " busy", null)).toString();
		
		return (new Message(Costanti.okCode, "Sfida a " + nickAmico + " inviata. In attesa di accettazione", null)).toString();
	}
	
	// Metodo per recuperare il punteggio dell'utente
	public String punteggioUtente(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, nickUtente + " non registrato", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return (new Message(Costanti.errorCode, nickUtente + " offline", null)).toString();
		
		return (new Message(Costanti.okCode, Costanti.PUNTEGGIO, u.getStatPunteggio())).toString();
	}
	
	// Metodo per recuperare la classifica dell'utente
	public synchronized String classificaUtente(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return (new Message(Costanti.errorCode, nickUtente + " non registrato", null)).toString();
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return (new Message(Costanti.errorCode, nickUtente + " offline", null)).toString();
		
		// Creazione del JSONArray per la classifica
		JSONArray myRanking = new JSONArray();
		
		Set<String> lAmici = u.getAmici();
		
		// Lista ordinata in base al punteggio
		List<User> oList = new ArrayList<User>();
		
		for (String amico : lAmici) {
			oList.add(this.db.get(amico));
		}
			
		oList.add(u);
			
		// Riordino degli utenti in base al punteggio
		Collections.sort(oList, (o1, o2) -> o1.getPunteggio() - o2.getPunteggio());
		Collections.reverse(oList);
		for (User amico : oList) {
			HashMap<String,Object> additionalDetails = new HashMap<String,Object>();

			additionalDetails.put(Costanti.NICKAMICO, amico.getNickUtente());
			additionalDetails.put(Costanti.PUNTEGGIO, amico.getPunteggio());
			
			JSONObject utente = new JSONObject(additionalDetails);
			
			myRanking.add(utente);
		}
		
		return (new Message(Costanti.okCode, myRanking.toJSONString(), null)).toString();
	}
	
	// Metodo che salva il database come file json
	private synchronized void saveJSONFile() {
		try {
			Gson gson = new Gson();
			String json = gson.toJson(db); 
			
			ByteBuffer buffer = ByteBuffer.wrap(json.getBytes());
			
			// Cancello il file se esite
			Files.deleteIfExists(Paths.get(fileName)); 
			
			// Creazione del nuovo file
			Files.createFile(Paths.get(fileName)); 
			
			// Creazione del Channel per la scrittura
			FileChannel outChannel = FileChannel.open(Paths.get(fileName), StandardOpenOption.WRITE);
			
			while(buffer.hasRemaining()) {
				outChannel.write(buffer);
			}
			
			outChannel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Metodo che recupera il file json
	private synchronized void retrieveJSONFile() {
        File f = new File(fileName); 
  
        if (!f.exists()) 
        	return;
        
		try {
			FileChannel inChannel =  FileChannel.open(Paths.get(fileName), StandardOpenOption.READ);
			String json = "";
			ByteBuffer buffer = ByteBuffer.allocate(1024*1024*100);
            boolean stop=false;
	
            // Lettura del file con il channel di input 
            while (!stop) {
				int bytesRead = inChannel.read(buffer);
				if (bytesRead == -1) stop = true;
				buffer.flip();
				while (buffer.hasRemaining())
					json += StandardCharsets.US_ASCII.decode(buffer).toString();
				buffer.clear();
			}
            
			inChannel.close();
			
			Gson gson = new Gson();
			Type type = new TypeToken<HashMap<String, User>>(){}.getType();
			db = gson.fromJson(json, type);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	// Metodo per dichiarare che l'utente nickUtente è occupato per una sfida
	public synchronized void setBusy(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return;
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return;
		
		// Si imposta a occupato
		u.setBusy(true);
	}
	
	// Metodo per dichiarare che l'utente nickUtente è disponibile per una sfida
	public synchronized void setFree(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return;
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return;
		
		// Si libera l'utente
		u.setBusy(false);
	}

	// Metodo per aggiornare punteggio e le altre informazioni inerenti all'esito di una sfida
	public synchronized void updatePunteggio(ChallengeInfo stat) {
		// In questo metodo non si verifica che l'utente sia online
		// Perchò potrebbe aver terminato il client senza aver concluso la sfida e senza aver fatto logout
		
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(stat.getNickname()))
			return;
		
		User u = this.db.get(stat.getNickname());
		
		// Aggiornamento delle informazioni
		u.setPunteggio(stat.calcolaPunti());
		u.setPunteggio(stat.getExtra());
		u.incrSfideGiocate();
		u.setCorrette(stat.getCorrette());
		u.setErrate(stat.getSbagliate());
		u.setNonFornite(stat.getInBianco());
		
		// Si salva il db su file in formato JSON
		saveJSONFile();
	}

	// Metodo per recuperare la socket della sessione attuale dell'utente nickUtente
	public Socket getClientSocket(String nickUtente) {
		// Si controlla se nickUtente è registrato al servizio
		if (!this.db.containsKey(nickUtente))
			return null;
		
		User u = this.db.get(nickUtente);
		
		// Si controlla se nickUtente è online
		if (!u.isOnline()) 
			return null;
		
		return u.getClientSocket();
	}
	
	// Metodo per impostare il servizio di sfida a ON
	public synchronized void setOnSfidaService() {
		this.sfidaService = true;
	}
	
	// Metodo per impostare il servizio di sfida a OFF
	public synchronized void setOffSfidaService() {
		this.sfidaService = false;
	}
}
