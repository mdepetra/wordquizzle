/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza le informazioni di un utente per una sfida
public class ChallengeInfo {
	private int corrette;		// Numero di traduzioni corrette
	private int sbagliate;		// Numero di traduzioni sbagliate
	private int inBianco;		// Numero di traduzioni lasciate in bianco
	private int extra;			// Punti extra
	private String nickname;	// Nome dell'utente
	private String msg;			// Messaggio ricevuto
	private int i;				// Indice della prossima parola da inviare
	private boolean ended;		// Flag: true -> l'utente ha finito le parole; false -> altrimenti
	
	// Costruttore
	public ChallengeInfo () {
		this.msg = "";
		this.corrette = this.sbagliate = this.inBianco = this.extra = 0;
		this.i = 0;
		this.ended = false;
	}
	
	// Metodo get per i punti extra
	public int getExtra() {
		return extra;
	}
	
	// Metodo set per i punti extra
	public void setExtra() {
		this.extra = Costanti.pExtraZ;
	}
	
	// Metodo get per il numero di risposte corrette
	public int getCorrette() {
		return corrette;
	}
	
	// Metodo per incrementare il numero di risposte corrette
	public void incrCorrette() {
		this.corrette++;
	}
	
	// Metodo get per il numero di risposte sbagliate
	public int getSbagliate() {
		return sbagliate;
	}
	
	// Metodo per incrementare il numero di risposte sbagliate
	public void incrSbagliate() {
		this.sbagliate++;
	}
	
	// Metodo get per il numero di risposte lasciate in bianco
	public int getInBianco() {
		return inBianco;
	}
	
	// Metodo per incrementare il numero di risposte lasciate in bianco
	public void incrInBianco() {
		this.inBianco++;
	}

	// Metodo get per il nome utente
	public String getNickname() {
		return nickname;
	}

	// Metodo set per il nome utente
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	// Metodo get per il messaggio
	public String getMsg() {
		return msg;
	}

	// Metodo set per il messaggio
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	// Metodo set per il termine delle parole
	public void setEnded() {
		this.ended = true;
	}
	
	// Metodo get per il termine delle parole
	public boolean getEnded() {
		return this.ended;
	}

	// Metodo che calcola i punti ottenuti dall'utente
	public int calcolaPunti() {
		return Costanti.pRCorrettaX*getCorrette() + Costanti.pRSbagliataY*getSbagliate() + Costanti.pInBianco*getInBianco();
	}
	
	// Metodo che incremente l'indice della parola a cui l'utente è arrivato
	public void incrIndex() {
		this.i++;
	}
	
	// Metodo get per recuperare l'indice della parola a cui l'utente è arrivato
	public int getIndex() {
		return i;
	}
}
