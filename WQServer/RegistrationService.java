import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

public interface RegistrationService extends Remote {

	public String registra_utente(String nickUtente, String password) throws RemoteException;
}
