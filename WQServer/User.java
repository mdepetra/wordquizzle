import java.net.Socket;
import java.util.HashMap;
import java.util.Set;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza un utente di WordQuizzle
public class User {
	private String nickUtente;	// Nome utente univoco che identifica uno specifico utente registrato al servizio
	private String password;	// Permette di verificare l'identità dell'utente
	private int punteggio;		// Numero di punti realizzati dall'utente nelle sfide
	private Set<String> amici;	// Insieme che riporta le relazioni di amicizia dell'utente
	private int sfideGiocate;	// Numero di sfide giocate dall'utente
	private int kCorrette;		// Numero di risposte corrette fornite dall'utente
	private int kErrate;		// Numero di risposte errate fornite dall'utente
	private int kNonFornite;	// Numero di risposte "saltate" fornite dall'utente
	private transient boolean online;	// booleano che indica lo status dell'utente (online/offline) 
	private transient boolean busy;		// booleano che indica lo status dell'utente per partecipare ad una sfida (free/busy)
	private transient Socket clientSocket; // socket per la sessione in corso dell'utente
	
	// Costruttore
	public User(String nickUtente, String password, int punteggio, int sfideGiocate, int kCorrette, int kErrate, int kNonFornite, Set<String> amici) {
		this.nickUtente = nickUtente;
		this.password = password;
		this.punteggio = punteggio;
		this.sfideGiocate = sfideGiocate;
		this.kCorrette = kCorrette;
		this.kErrate = kErrate;
		this.kNonFornite = kNonFornite;
		this.amici = amici;
		this.setOnline(false);
		this.setBusy(false);
	}
	
	// Metodo per restituire il nickUtente
	public String getNickUtente() {
		return this.nickUtente;
	}
	
	// Metodo per restituire le statistische e il punteggio dell'utente
	public HashMap<String, String> getStatPunteggio() {
		HashMap<String, String> attr = new HashMap<String, String>();
		attr.put(Costanti.PUNTEGGIO, Integer.toString(punteggio));
		attr.put(Costanti.SFIDEGIOCATE, Integer.toString(sfideGiocate));
		attr.put(Costanti.CORRETTE, Integer.toString(kCorrette));
		attr.put(Costanti.ERRATE, Integer.toString(kErrate));
		attr.put(Costanti.NONFORNITE, Integer.toString(kNonFornite));
		return attr;
	}
	
	// Metodo per restituire il punteggio dell'utente (per la classifica)
	public int getPunteggio() {
		return punteggio;
	}
	
	// Metodo per aggiornare il punteggio dell'utente
	public void setPunteggio(int punti) {
		this.punteggio += punti;
	}
	
	// Metodo per incrementare il numero di sfide disputate dall'utente
	public void incrSfideGiocate() {
		this.sfideGiocate++;
	}
	
	// Metodo per aggiornare il numero di traduzioni corrette fornite dall'utente in tutte le sfide
	public void setCorrette(int k) {
		this.kCorrette += k;
	}
	
	// Metodo per aggiornare il numero di traduzioni errate fornite dall'utente in tutte le sfide
	public void setErrate(int k) {
		this.kErrate += k;
	}
	
	// Metodo per aggiornare il numero di traduzioni non fornite (cioè inviate in bianco) dall'utente in tutte le sfide
	public void setNonFornite(int k) {
		this.kNonFornite += k;
	}
	
	// Metodo per recuperare gli amici dell'utente
	public Set<String> getAmici() {
		return this.amici;
	}

	// Metodo che verifica se la password è corretta
	public boolean verificaID(String password) {
		return this.password.equals(password);
	}

	// Metodo che aggiunge nickAmico agli amici
	public boolean aggiungiAmico(String nickAmico) {
		return this.amici.add(nickAmico);
	}

	// Metodo che verifica se nickAmico è tra gli amici
	public boolean isAmico(String nickAmico) {
		return this.amici.contains(nickAmico);
	}

	// Metodo per verificare se l'utente è online o offline
	public boolean isOnline() {
		return online;
	}

	// Metodo per impostare lo status dell'utente (online o offline)
	public void setOnline(boolean online) {
		this.online = online;
	}

	// Metodo per verificare se l'utente è busy o free
	public boolean isBusy() {
		return busy;
	}

	// Metodo per impostare lo status dell'utente (busy o free)
	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	// Metodo per impostare la socket del client
	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}
	
	// Metodo per recuperare la socket del client
	public Socket getClientSocket() {
		return this.clientSocket;
	}
	
}
