import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza il gestore del client
public class ClientHandler implements Runnable {
	private Socket clientSocket;	// Socket per comunicare con il client
	private BufferedWriter writer;	// Per scrivere
	private BufferedReader reader;	// Per leggere
	private boolean isConnected;	// Flag di connessione del client
	String nickUtente;				// Nome dell'utente
	private Database db;			// Database del sistema WordQuizzle
	
	// Costruttore
	public ClientHandler(Socket clientSocket, Database db) {
		this.clientSocket = clientSocket;
		this.writer = null;
		this.reader = null;
		this.isConnected = true;
		this.db = db;
	}

	@Override
	public void run() {
		try {
			// Creazione del lettore sulla socket
			reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			
			while (isConnected) {
				// Lettura del messaggio di richiesta
				String request = reader.readLine();
				
				// Ricezione del messaggio di richiesta
				Message msg = new Message().messageFromString(request);
				
				String resp = null;
				
				// Elaborazione degli attributi del messaggio ricevuto
		        HashMap<String, String> attr = msg.getAttributi();
		        
		        System.out.println("WQServer | " + Thread.currentThread().getName() + " ha ricevuto il messaggio " + request);
		        
				// Elaborazione dell'operazione da effettuare sulla base del codice del messaggio ricevuto
				switch (msg.getCode()) {
					case Costanti.LOGIN:
						nickUtente = attr.get(Costanti.NICKUTENTE);
						System.out.println("WQServer | " + Thread.currentThread().getName() + " gestisce " + nickUtente);
						
						resp = db.loginUtente(nickUtente, attr.get(Costanti.PASSWORD), clientSocket);
						if (!resp.contains(Costanti.okCode)) 
							nickUtente = null;
						break;
					case Costanti.LOGOUT:
						resp = db.logoutUtente(nickUtente);
						if (resp.contains(Costanti.okCode))
							isConnected = false;
						break;
					case Costanti.AGGIUNGI_AMICO:
						resp = db.aggiungiAmico(nickUtente, attr.get(Costanti.NICKAMICO));
						break;
					case Costanti.LISTA_AMICI:
						resp = db.listaAmici(nickUtente);
						break;
					case Costanti.SFIDA:
						resp = db.sfida(nickUtente, attr.get(Costanti.NICKAMICO));
						if (resp.contains(Costanti.okCode)) 
							setUpChallengeHandler(attr.get(Costanti.NICKAMICO));
						break;
					case Costanti.MOSTRA_PUNTEGGIO:
						resp = db.punteggioUtente(nickUtente);
						break;
					case Costanti.MOSTRA_CLASSIFICA:
						resp = db.classificaUtente(nickUtente);
						break;
					default:
						// Caso di default se il codice operazione non è presente
						resp = (new Message(Costanti.errorCode, "Comando non riconosciuto.", null)).toString();
						break;
				}
				
				// Invio del messaggio di risposta
				if (resp != null) {
					sendResponse(resp);
				}
			}
		} catch (NullPointerException | IOException e) {
			// In caso di errore: effettuo il logout, la chiusura della socket e cambio il booleano isConnected
			db.logoutUtente(nickUtente);
			isConnected = false;
		}
		
		System.out.println("WQServer | ClientHandler termina " + Thread.currentThread().getName());
	}
	
	// Metodo per inviare i messaggi di risposta al client
	public void sendResponse(String resp) throws IOException {
		writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
		writer.write(resp);
		writer.newLine();
		writer.flush();
		System.out.println("WQServer | " + Thread.currentThread().getName() + " invia a " + nickUtente + ":\n\t\t" + resp);
	}

	// Metodo per avviare un thread per gestire l'inoltro della richiesta di sfida e l'eventuale sfida in caso di accettazione
	public void setUpChallengeHandler(String nickAmico) {
		System.out.println("WQServer | " + Thread.currentThread().getName() + "\n\t\t" + nickUtente + " ha lanciato una sfida " + nickAmico);
		ChallengeHandler challengeHandler = new ChallengeHandler(nickUtente, nickAmico, db);
        Thread thread = new Thread(challengeHandler);
        thread.start();
	}
}
