import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modellizza i messaggi scambiati tra client e server
public class Message {
	private String code;	// stringa che identifica una specifica operazione o un determinato esito
	private String messageBody;	// corpo del messaggio
	private HashMap<String,String> attributi;	// una struttura che memorizza gli eventuali attributi aggiuntivi al messaggio. Sono rappresentati come coppie di stringhe <chiave,valore>.

	// Costruttore
	public Message(String code, String messageBody, HashMap<String, String> attributi) {
		this.setCode(code);
		this.setMessageBody(messageBody);
		this.setAttributi(attributi);
	}
	
	// Costruttore vuoto
	public Message() {}

	// Metodo che riceve una stringa e usa la libreria Gson per trasformarla in oggetto Message
	public Message messageFromString(String string) {
		Gson gson = new Gson();
		Type type = new TypeToken<Message>(){}.getType();
		Message msg = gson.fromJson(string, type);
		
		return msg;
	}
	
	// Metodo che usa la libreria Gson per trasformare l’oggetto (il messaggio) in JSON.
	public String toString() { 
		Gson gson = new Gson();
	    return gson.toJson(this);
	}

	// Metodo per ottenere il corpo del messaggio
	public String getMessageBody() {
		return messageBody;
	}

	// Metodo per inserire il corpo del messaggio
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	// Metodo per ottenere il codice del messaggio
	public String getCode() {
		return code;
	}

	// Metodo per inserire il codice del messaggio
	public void setCode(String code) {
		this.code = code;
	}

	// Metodo per ottenere gli attributi del messaggio
	public HashMap<String,String> getAttributi() {
		return attributi;
	}

	// Metodo per inserire gli attribudi del messaggio
	public void setAttributi(HashMap<String,String> attributi) {
		this.attributi = attributi;
	}
}
