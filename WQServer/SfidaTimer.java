/**
 * 
 * Word Quizzle
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza il tempo di una sfida
public class SfidaTimer implements Runnable{

	// Costruttore
	public SfidaTimer() {}

	@Override
	public void run() {
		try {
			Thread.sleep(Costanti.T2);
			System.out.println("WQServer | La sfida sta terminando.");
		} catch (InterruptedException e) {
			System.out.println("WQServer | Thread Timer Sfida Interrotto"); 
		}
	}

	
}
